package com.ztl.usercenter02.service;

import com.ztl.usercenter02.model.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;
import jakarta.servlet.http.HttpServletRequest;

/**
* @author --
* @description 针对表【user(用户表)】的数据库操作Service
* @createDate 2024-04-11 15:36:34
*/
public interface UserService extends IService<User> {

    /**
     * 用户注册
     * @param userAccount 用户账户
     * @param userPassword 用户密码
     * @param checkPassword 确认密码
     * @return id
     */
    long userRegister(String userAccount, String userPassword, String checkPassword);

    /**
     * 用户登录
     *
     * @param userAccount  用户账户
     * @param userPassword 用户密码
     * @param request 拿到session
     * @return 脱敏后的用户信息
     */
    User userLogin(String userAccount, String userPassword, HttpServletRequest request);

    /**
     * 数据脱敏
     * @param user 需要脱敏的数据
     * @return 脱敏后的数据
     */
    User saftyUser(User user);

    /**
     *
     * @param request 拿session
     * @return
     */
    int userLoginOut(HttpServletRequest request);
}
