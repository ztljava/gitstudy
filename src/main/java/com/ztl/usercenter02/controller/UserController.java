package com.ztl.usercenter02.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ztl.usercenter02.common.BaseResponse;
import com.ztl.usercenter02.common.ErrorCode;
import com.ztl.usercenter02.common.ResultUtils;
import com.ztl.usercenter02.exception.BusinessException;
import com.ztl.usercenter02.model.domain.User;
import com.ztl.usercenter02.model.request.UserRequest;
import com.ztl.usercenter02.service.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.ztl.usercenter02.constant.UserConstant.ADMIN_ROLE;
import static com.ztl.usercenter02.constant.UserConstant.USER_LOGIN_STATE;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/register")
    public BaseResponse<Long> userRegister(@RequestBody UserRequest userRegisterRequest){
        if (userRegisterRequest == null) {
           throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }


        String userAccount = userRegisterRequest.getUserAccount();
        String userPassword = userRegisterRequest.getUserPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        if(StringUtils.isAnyBlank(userPassword, userAccount, checkPassword)){
            return null;
        }

        long result = userService.userRegister(userAccount, userPassword, checkPassword);
        return ResultUtils.success(result);
    }

    @GetMapping("/current")
    public BaseResponse<User> getCurrentUser(HttpServletRequest request){
        Object userObject = request.getSession().getAttribute(USER_LOGIN_STATE);
        User user = (User)userObject;
        if (user == null) {
            return null;
        }
        User currentUser = userService.getById(user.getId());
        User saftyUser = userService.saftyUser(currentUser);
        return ResultUtils.success(saftyUser);

    }
    @PostMapping("/login")
    public BaseResponse<User> userLogin(@RequestBody UserRequest userLoginRequest, HttpServletRequest request){

        if (userLoginRequest == null) {
            return null;
        }

        String userAccount = userLoginRequest.getUserAccount();
        String userPassword = userLoginRequest.getUserPassword();

        if(StringUtils.isAnyBlank(userPassword, userAccount)){
            return null;
        }

        User saftyUser = userService.saftyUser(userService.userLogin(userAccount, userPassword, request));
        return ResultUtils.success(saftyUser);

    }

    @PostMapping("/loginout")
    public BaseResponse<Integer> userLoginOut(HttpServletRequest request){

        if (request == null) {
            return null;
        }

        int result = userService.userLoginOut(request);
        return ResultUtils.success(result);

    }

    @GetMapping("/search")
    public BaseResponse<List<User>> searchUsers(String username, HttpServletRequest request){


        if(!isAdmin(request)){
            return null;
        }

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(username)){
            queryWrapper.like("username",username);
        }
        List<User> userList = userService.list(queryWrapper);
        List<User> list = userList.stream().map(user -> userService.saftyUser(user)).collect(Collectors.toList());
        return ResultUtils.success(list);
    }

    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteUser(long id, HttpServletRequest request){
        if(!isAdmin(request)){
            return null;
        }
        if (id <= 0){
            return null;
        }
        boolean b = userService.removeById(id);
        return ResultUtils.success(b);
    }

    private boolean isAdmin(HttpServletRequest request){
        Object userObject = request.getSession().getAttribute(USER_LOGIN_STATE);
        User user = (User)userObject;
        if (user == null || user.getUserRole() != ADMIN_ROLE) {
            return false;
        }
        return true;
    }
}
