package com.ztl.usercenter02.model.request;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class UserRequest implements Serializable {


    @Serial
    private static final long serialVersionUID = 4680229045028675060L;

    private String userAccount;
    private String userPassword;
    private String checkPassword;
}
