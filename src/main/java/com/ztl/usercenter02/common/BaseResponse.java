package com.ztl.usercenter02.common;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 通用返回类
 * @param <T>
 *
 * @author ztl
 */
@Data
public class BaseResponse<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = 2516700673946299652L;

    private Integer code;

    private T data;

    private String message;

    private String description;
    public BaseResponse(Integer code, T data, String message, String description) {
        this.code = code;
        this.data = data;
        this.message = message;
        this.description = description;
    }
    public BaseResponse(Integer code, T data) {

        this(code, data, "", "");
    }

    public BaseResponse(Integer code, T data, String message) {

        this(code, data, message, "");
    }

    public BaseResponse(ErrorCode errorCode){
        this(errorCode.getCode(), null, errorCode.getMessage(), errorCode.getDescription());
    }


}
