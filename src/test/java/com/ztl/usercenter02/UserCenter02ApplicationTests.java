package com.ztl.usercenter02;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.DigestUtils;

@SpringBootTest
class UserCenter02ApplicationTests {

    @Test
    void testDigest(){
        String newPassword = DigestUtils.md5DigestAsHex(("abc" + "myspassword").getBytes());
        System.out.println(newPassword);

    }
//    @Resource
//    private UserMapper userMapper;

//    @Test
//    public void testSelect() {
//        System.out.println(("----- selectAll method test ------"));
//        List<User> userList = userMapper.selectList(null);
//        Assert.isTrue(5 == userList.size(), "");
//        userList.forEach(System.out::println);
//    }

}
